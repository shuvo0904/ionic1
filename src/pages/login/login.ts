import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController  } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { AuthProvider } from '../../providers/auth/auth';
import { TabsPage } from '../tabs/tabs';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

	username: string = '';
	password: string = '';
	errorMsg:string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, 
    public alert: AlertController, public authService:AuthProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  errorFunc(message){
  	let alert = this.alert.create({
  		title : 'Warining!',
  		subTitle : message,
  		buttons: ['Ok']
  	});
  	alert.present();
  }

  myLogIn(){
    if (this.username.trim() !==''    ) {
      console.log(this.username.trim() + "   " + this.password.trim() )

      if (this.password.trim()  === '') {
        this.errorFunc('Please put your password')

      }else{

       // let credentials = {
       //   username: this.username,
       //   password: this.password,
      //    grant_type: "password"
      //  };
      let credentials = "userName="+this.username+"&password="+this.password+"&grant_type=password";

         this.authService.login(credentials).then((result) => {
            console.log(result);
            this.navCtrl.setRoot(TabsPage);

        }, (err) => {
                 console.log(err);
            this. errorFunc('Wrong credentials ! try again')
            console.log("credentials: "+JSON.stringify(credentials))

        });

      }

   }
   else{

    this. errorFunc('Please put a vaild password !')

    }

}





myLogout(){
  this.authService.logout();
}

}
