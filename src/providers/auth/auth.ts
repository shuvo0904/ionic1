
import { Injectable } from '@angular/core';


import { Storage } from '@ionic/storage';
//import { apikey } from '../../app/apiurls/serverurls.js';
import {Http, Headers} from '@angular/http';

/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

public token: any;

  //private headers: Headers = new Headers();

  constructor(public storage: Storage, public http:Http) {
    console.log('Hello AuthProvider Provider');
  }


  createAccount(details){
  	return new Promise((resolve, reject)=>{
  		let headers: Headers= new Headers();
    //  headers.append('Access-Control-Allow-Origin' , '*');
    //  headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
  		headers.append('Content-Type','application/x-www-form-urlencoded');
  		this.http.post('http://192.168.56.1:81/api/Account/Register',details,{headers:headers})
  		.subscribe(res=>{
        console.log("success",res);
  		//	let data = res.json();
  			//resolve(data);
  		},(err)=>{
         console.log("err",err);
  			reject(err);
  		})
  	})
  }

  login(credentials){
  	return new Promise((resolve, reject)=>{
  		 let headers: Headers= new Headers();
      //headers.append('Access-Control-Allow-Origin' , '*');
       // headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
       // headers.append('Accept','application/json');
        headers.append('Content-Type','application/x-www-form-urlencoded');

        console.log(JSON.stringify(credentials));

  		this.http.post('http://192.168.56.1:81/token',credentials,{headers:headers})
  		.subscribe(res=>{
  			let data = res.json();
  			this.token = data.access_token;
  			this.storage.set('token',data.access_token);
  			resolve(data);
  		},(err)=>{
  			reject(err);
  		})
  	})
  }


 checkAuthentication(){

    return new Promise((resolve, reject) => {
      this.storage.get('token').then((value) => {
      this.token = value;
      console.log("NewToken",value);
      resolve(this.token)

    }) 
  });        

  }




  logout(){
    this.storage.set('token', '');
   }

}
